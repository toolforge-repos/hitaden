// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  modules: ['@nuxtjs/tailwindcss'],
  devtools: { enabled: true },
  css: ['~/assets/css/global.css'],
  ssr: true,
  nitro: {
    timing: true,
  },
  runtimeConfig: { // Overriden by NUXT_* environment variables on Toolforge, see README.md
    dbHostname: '',
    dbPort: '',
    toolReplicaUser: '',
    toolReplicaPassword: '',
  },
  build: {
    transpile: ['echarts', 'zrender'],
  },
  app: {
    head: {
      title: 'Hitaden',
      meta: [
        { charset: 'utf-8' },
        { name: 'author', content: 'Lofhi' },
        { name: 'keywords', content: 'Wikipedia, Wikipedia, commentaires, contributions, Hitaden' },
        { name: 'lang', content: 'fr' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        { hid: 'description', name: 'description', content: 'Hitaden est un service Web communautaire permettant de rechercher des commentaires de contributions sur Wikipédia.' }
      ],
    },
  },
  routeRules: {
    '/': { prerender : true },
    '/about': { swr : true },
  },
  vite:{
    vue:{
      script: {
        defineModel: true,
      }
    }
  }
})