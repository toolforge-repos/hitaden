export type Edition = {
    label: string;
    value: string;
    description: string;
};