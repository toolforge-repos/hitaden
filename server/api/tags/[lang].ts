interface ParamsContext {
  lang?: string;
}

export default defineEventHandler(async (event) => {
  const params: ParamsContext = event.context.params || {};

  if (!params || !params.lang || params.lang === '') {
    setResponseStatus(event, 400);
    return;
  }

  const lang: string = params.lang;

  const data = await fetchWithContinue<TagItem>(
    `https://${lang}.wikipedia.org/w/api.php`,
    {
      action: 'query',
      list: 'tags',
      format: 'json',
      tglimit: '500'
    }
  );

  return new Response(JSON.stringify(data), { headers: { 'Content-Type': 'application/json' } });
})


interface QueryResult<T> {
  batchcomplete: string;
  continue?: {
    tgcontinue?: string;
    continue?: string;
  };
  error?: string;
  query: {
    tags: TagItem[];
  };
}

interface TagItem {
  name: string;
}

const fetchWithContinue = async <T>(url: string, params: Record<string, string>): Promise<TagItem[]> => {
  const fetchData = async (innerContinueParams?: Record<string, string>): Promise<QueryResult<T>> => {
    const queryParams = new URLSearchParams({ ...params, ...innerContinueParams });
    const fullUrl = `${url}?${queryParams.toString()}`;

    const response = await fetch(fullUrl, {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    });

    const result: QueryResult<TagItem> = await response.json();

    if ('error' in result) console.log(result.error);
    if ('warnings' in result) console.log(result.warnings);

    return result;
  };

  let data: TagItem[] = [];
  let tgcontinue: string | undefined;

  do {
    const result = await fetchData(tgcontinue ? { tgcontinue } : undefined);

    if ('continue' in result) {
      tgcontinue = result.continue?.tgcontinue;
    } else {
      tgcontinue = undefined;
    }

    if (result.query.tags.length > 0) {
      data.push(...result.query.tags);
    }

  } while (tgcontinue);

  return data;
};