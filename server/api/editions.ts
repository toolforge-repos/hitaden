import { Edition } from "~/types/types";

export default defineEventHandler(async (event) => {
    interface InterwikiItem {
        prefix: string;
        local: string;
        url: string;
        language?: string;
        bcp47?: string;
    }

    interface QueryResult {
        batchcomplete: string;
        query: {
            interwikimap: InterwikiItem[];
        };
    }

    let fetchRequest: QueryResult = await fetch(
        "https://fr.wikipedia.org/w/api.php?action=query&meta=siteinfo&sifilteriw=local&siprop=interwikimap&format=json",
    ).then((res) => res.json());

    const interwikimap: InterwikiItem[] = fetchRequest.query.interwikimap;

    let data: Edition[] = interwikimap.filter((item: { url: string; prefix: string; }) => item.url.includes(".wikipedia.org/wiki/$1") && item.prefix === item.url.split(".")[0].split("://")[1])
        .map((item) => {
            const domain = item.url.split("://")[1].split("/")[0];
            return { label: item.language ? item.language.toLocaleLowerCase() : item.prefix, value: domain, description: domain};
        });

    return data;
})
