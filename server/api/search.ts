import dayjs, { Dayjs } from 'dayjs';
import utc from 'dayjs/plugin/utc.js';
import { Edition } from "~/types/types";
import mysql, { RowDataPacket } from 'mysql2/promise';
import { z } from 'zod'
import sanitizeHtml from 'sanitize-html';

dayjs.extend(utc);

const searchSchema = z.object({
  search: z.string().min(1).max(255),
  edition: z.string().regex(/([^\.]+)\.wikipedia\.org/gmi),
  tag: z.string().default(''),
  startDate: z.date(),
  endDate: z.date()
})

const runtimeConfig = useRuntimeConfig();

export default defineEventHandler(async (event) => {
  const result = await readValidatedBody(event, body => searchSchema.safeParse(body))

  if (!result.success) {
    //setResponseStatus(event, 400);
    //return;
  }

  const data = await readBody(event);
  const search: string = data.search || "";
  const edition: string = data.edition || ""
  const startDate: Dayjs = dayjs(data.startDate).isValid() ? dayjs(data.startDate) : dayjs().subtract(14, 'days').utc();
  const endDate: Dayjs = dayjs(data.endDate).isValid() ? dayjs(data.endDate) : dayjs().utc();
  const tag: string = data.tag || "";
  const editionsR: Edition[] = await $fetch('/api/editions');

  if (editionsR.flatMap(item => item.value).indexOf(edition) === -1) {
    setResponseStatus(event, 400);
    return;
  }

  if (endDate.isBefore(startDate) || endDate.diff(startDate, 'month', true) > 3) {
    setResponseStatus(event, 400);
    return;
  }

  const dbName = data.edition.split('.')[0] + "wiki";
  const startDateString: string = startDate.format().slice(0, 10).replace(/-/g, '') + '000000' // 20230910000000
  const endDateString: string = endDate.format().slice(0, 10).replace(/-/g, '') + '235959' // 20230910235959

  const DB_HOSTNAME = process.env.NODE_ENV === 'production' ? `${dbName}${runtimeConfig.dbHostname}` : runtimeConfig.dbHostname;
  const DB_PORT = runtimeConfig.dbPort !== '' ? `:${runtimeConfig.dbPort}` : '';
  const DB_NAME = process.env.NODE_ENV === 'production' ? `${dbName}_p` : 'frwiki_p';

  let [rows, fields] = [[], []];

  try {
    const connection = await mysql.createConnection(
      `mysql://${runtimeConfig.toolReplicaUser}:${runtimeConfig.toolReplicaPassword}@${DB_HOSTNAME}${DB_PORT}/${DB_NAME}`
    );
    const sql = `SELECT ar.actor_name, p.page_id, p.page_title, p.page_namespace, rev.rev_id, rev.rev_timestamp, com.comment_text
    FROM comment_revision AS com
    INNER JOIN revision AS rev ON com.comment_id = rev.rev_comment_id
    INNER JOIN page AS p ON rev.rev_page = p.page_id
    INNER JOIN actor AS ar ON rev.rev_actor = ar.actor_id
    WHERE 
        rev.rev_timestamp >= ?
        AND rev.rev_timestamp <= ?
        AND com.comment_text LIKE ?;`;
    console.log(startDateString, endDateString, `%${search}%`);
    const values = [startDateString, endDateString, `%${search}%`];
    const [rows, fields] = await connection.execute(sql, values);
    const transformedRows: any[] = (rows as RowDataPacket[]).map(row => {
      return {
        actor_name: row.actor_name.toString(),
        page_id: row.page_id,
        page_title: row.page_title.toString().replace(/_/g, ' '),
        page_namespace: namespaceToString(row.page_namespace),
        rev_timestamp: row.rev_timestamp.toString(),
        rev_id: row.rev_id,
        comment_text: sanitizeHtml(row.comment_text.toString(), {
          allowedTags: [],
          allowedAttributes: {},
          disallowedTagsMode: 'escape'
        }).replace(search, `<mark>${search}</mark>`),
      };
    });
    connection.destroy();
    return transformedRows;

  } catch (err) {
    console.log(err);
    setResponseStatus(event, 400);
    return;
  }
})
function namespaceToString(page_namespace: string): string {
  switch (page_namespace) {
    case '0':
      return 'Article';
    case '1':
      return 'Talk';
    case '2':
      return 'User';
    case '3':
      return 'User talk';
    case '4':
      return 'Wikipedia';
    case '5':
      return 'Wikipedia talk';
    case '6':
      return 'File';
    case '7':
      return 'File talk';
    case '8':
      return 'MediaWiki';
    case '9':
      return 'MediaWiki talk';
    case '10':
      return 'Template';
    case '11':
      return 'Template talk';
    case '12':
      return 'Help';
    case '13':
      return 'Help talk';
    case '14':
      return 'Category';
    case '15':
      return 'Category talk';
    default:
      return 'Autre';
  }
}

