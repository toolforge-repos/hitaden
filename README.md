# Hitaden

Hitaden is a community web service to search for words in Wikipedia edits comments. It's hosted on [Toolforge](https://wikitech.wikimedia.org/wiki/Help:Toolforge), a free cloud hosting platform for Wikimedia contributors part of the [Wikimedia Cloud Services](https://wikitech.wikimedia.org/wiki/Help:Cloud_Services_introduction) (WMCS). The WMCS are supported by Wikimedia Foundation staff and volunteers.

## Technologies used

- [Codex](https://doc.wikimedia.org/codex/latest/), the new design system for the projects hosted by Wikimedia Foundation
- [Vue 3](https://vuejs.org/), the web interface framework (with TypeScript, composition API and `script setup`)
- [Nuxt 3](https://nuxtjs.org/), a Vue framework to build complete web projects
- [Vue-ECharts](https://vue-echarts.dev/), a Vue wrapper for [Apache ECharts](https://echarts.apache.org/) to create interactive charts
- [Day.js](https://day.js.org/), a minimalist datetime library
- [zod](https://zod.dev/), a TypeScript-first schema declaration and validation library
- [sanitize-html](https://github.com/apostrophecms/sanitize-html), a whitelist-based HTML sanitizer
- [Tailwind CSS](https://tailwindcss.com/), a utility-first CSS framework
- [Node](https://nodejs.org/), the JavaScript runtime with [pnpm](https://pnpm.io/) as package manager
- [Nitro](https://nitro.unjs.io/), a framework to build web servers using [unjs/h3](https://h3.unjs.io/), a http server framework
- [Toolforge Build Service](https://wikitech.wikimedia.org/wiki/Help:Toolforge/Build_Service) (based on [Cloud Native Buildpacks](https://buildpacks.io/)) to build a container image and deploy the application on [Toolforge Kubernetes cluster](https://wikitech.wikimedia.org/wiki/Help:Toolforge/Kubernetes)
- [Toolforge Envvars Service](https://wikitech.wikimedia.org/wiki/Help:Toolforge/Envvars_Service) to set environment variables that will be available for the service when running

## Requirements

Make you sure to have Node.js 20.11.x and pnpm 8.15.x installed. You should use [nvm](https://github.com/nvm-sh/nvm) to manage Node versions. You could use npm, but pnpm is recommended. Only contributions using pnpm will be accepted.

**You also need to have a Wikimedia Developer account**, without it you will not be able to fetch contributions on replica databases. Since we are mainly using Toolforge, you can (and have to) read the [official instructions](https://wikitech.wikimedia.org/wiki/Help:Toolforge/Quickstart) for a quickstart. 

## Setup

1. You need to clone the project. You have some options:
   - Cloning the repository.:
     - With SSH: `git clone git@gitlab.wikimedia.org:toolforge-repos/hitaden.git`
     - Or with HTTPs : `git clone https://gitlab.wikimedia.org/toolforge-repos/hitaden.git`
     - Opening the repository directly in your IDE:
       - Visual Studio Code (and forks): [SSH](vscode://vscode.git/clone?url=git%40gitlab.wikimedia.org%3Atoolforge-repos%2Fhitaden.git) or [HTTPs](vscode://vscode.git/clone?url=https%3A%2F%2Fgitlab.wikimedia.org%2Ftoolforge-repos%2Fhitaden.git)
       - IntelliJ IDEA: [SSH](jetbrains://idea/checkout/git?idea.required.plugins.id=Git4Idea&checkout.repo=git%40gitlab.wikimedia.org%3Atoolforge-repos%2Fhitaden.git) or [HTTPs](jetbrains://idea/checkout/git?idea.required.plugins.id=Git4Idea&checkout.repo=https%3A%2F%2Fgitlab.wikimedia.org%2Ftoolforge-repos%2Fhitaden.git)
2. Then, you need to install the dependencies with `pnpm install`
3. Create a `.env.dev` file at the root of the project based on the `.env.dev.example` file. You should only edit `NUXT_TOOL_REPLICA_USER` and `NUXT_TOOL_REPLICA_PASSWORD` with your Toolforge tool credentials. The other variables are already set for development. You can retrieve the values of these vars **after creating a tool** with `toolforge envvars list` command when connected on Toolforge. Do not share these credentials with anyone.
4. At the moment wiki replicas are not public and you can access the database replicas from your own computer by setting up an SSH tunnel. By example, for *frwiki* : `ssh -L 4711:frwiki.web.db.svc.wikimedia.cloud:3306 login.toolforge.org`. This will pin the remote database on 3306 port to your local port 4711. You could also specify `yourusername@login.toolforge.org` to avoid typing your username.
5. (Optional) Make your life easier by properly setting your `.ssh\config` file. You should find plenty of examples for your operating system on the web. 
Here is an example for Windows:
```
Host login.toolsforge.org
  AddKeysToAgent yes
  ServerAliveInterval 20
  TCPKeepAlive no
  IdentityFile ~/.ssh/toolsforge
```
You could also simplify your life with similar settings for git if you are using SSH.
6. You can now start the development server with `pnpm run dev` and everything should be working fine.

## Commands

| Command            | Action                                                             |
| :----------------- | :----------------------------------------------------------------- |
| `pnpm run dev`     | Start the development server on `http://localhost:3000`            |
| `pnpm run build`   | Build the service for production                                   |
| `pnpm run preview` | Start a server to preview your application after the build command |

Others commands can be found in the [Nuxt documentation](https://nuxt.com/docs/api/commands/add).

## Deployment

TODO. A lot of things are working with magic, but this magic is not magic and with a bit of work you would understand the magic. 

Examples:

1. Webservices hosted on Toolforge will be passed an HTTP port to bind to in an environment variable named `PORT`. And Nitro, our server framework is using automatically [this variable](https://nitro.unjs.io/deploy/runtimes/node#environment-variables), passing it to h3 the http server framework
2. The buildpacks are building everything smoothly because we have `package.json` file and a `pnpm-lock.yaml` proprely set. Then the server is running because we have a `Profile` file with the right command. No one should also forget [`service.template`](https://wikitech.wikimedia.org/wiki/Help:Toolforge/Web#Webservice_templates).
3. Buildpacks are based on the work of Heroku, so a lot of details are hidden on Heroku's [documentation](https://devcenter.heroku.com/articles/buildpacks). It's worth to read it since you are not creating Dockerfiles or else, this process is now handled by the buildpack autobuilder.
